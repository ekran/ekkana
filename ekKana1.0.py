from tkinter import *
import random

hiragana = {
    "あ" : ["a"],
    "い" : ["i"],
    "う" : ["u"],
    "え" : ["e"],
    "お" : ["o"],
    "か" : ["ka"],
    "き" : ["ki"],
    "く" : ["ku"],
    "け" : ["ke"],
    "こ" : ["ko"],
    "さ" : ["sa"],
    "し" : ["shi"],
    "す" : ["su"],
    "せ" : ["se"],
    "そ" : ["so"],
    "た" : ["ta"],
    "ち" : ["chi"],
    "つ" : ["tsu"],
    "て" : ["te"],
    "と" : ["to"],
    "な" : ["na"],
    "に" : ["ni"],
    "ぬ" : ["nu"],
    "ね" : ["ne"],
    "の" : ["no"],
    "は" : ["ha"],
    "ひ" : ["hi"],
    "ふ" : ["fu"],
    "へ" : ["he"],
    "ほ" : ["ho"],
    "ま" : ["ma"],
    "み" : ["mi"],
    "む" : ["mu"],
    "め" : ["me"],
    "も" : ["mo"],
    "や" : ["ya"],
    "ゆ" : ["yu"],
    "よ" : ["yo"],
    "ら" : ["ra"],
    "り" : ["ri"],
    "る" : ["ru"],
    "れ" : ["re"],
    "ろ" : ["ro"],
    "わ" : ["wa"],
    "を" : ["wo"],
    "ん" : ["n"]
}

hiracrit = {
    "が" : ["ga"],
    "ぎ" : ["gi"],
    "ぐ" : ["gu"],
    "げ" : ["ge"],
    "ご" : ["go"],
    "ざ" : ["za"],
    "じ" : ["ji"],
    "ず" : ["zu"],
    "ぜ" : ["ze"],
    "ぞ" : ["zo"],
    "だ" : ["da"],
    "ぢ" : ["ji","dji","jyi"],
    "づ" : ["dzu","zu"],
    "で" : ["de"],
    "ど" : ["do"],
    "ば" : ["ba"],
    "び" : ["bi"],
    "ぶ" : ["bu"],
    "べ" : ["be"],
    "ぼ" : ["bo"],
    "ぱ" : ["pa"],
    "ぴ" : ["pi"],
    "ぷ" : ["pu"],
    "ぺ" : ["pe"],
    "ぽ" : ["po"]
}

katakana = {
    "ア" : ["a"],
    "イ" : ["i"],
    "ウ" : ["u"],
    "エ" : ["e"],
    "オ" : ["o"],
    "カ" : ["ka"],
    "キ" : ["ki"],
    "ク" : ["ku"],
    "ケ" : ["ke"],
    "コ" : ["ko"],
    "サ" : ["sa"],
    "シ" : ["shi"],
    "ス" : ["su"],
    "セ" : ["se"],
    "ソ" : ["so"],
    "タ" : ["ta"],
    "チ" : ["chi"],
    "ツ" : ["tsu"],
    "テ" : ["te"],
    "ト" : ["to"],
    "ナ" : ["na"],
    "ニ" : ["ni"],
    "ヌ" : ["nu"],
    "ネ" : ["ne"],
    "ノ" : ["no"],
    "ハ" : ["ha"],
    "ヒ" : ["hi"],
    "フ" : ["fu"],
    "ヘ" : ["he"],
    "ホ" : ["ho"],
    "マ" : ["ma"],
    "ミ" : ["mi"],
    "ム" : ["mu"],
    "メ" : ["me"],
    "モ" : ["mo"],
    "ヤ" : ["ya"],
    "ユ" : ["yu"],
    "ヨ" : ["yo"],
    "ラ" : ["ra"],
    "リ" : ["ri"],
    "ル" : ["ru"],
    "レ" : ["re"],
    "ロ" : ["ro"],
    "ワ" : ["wa"],
    "ヲ" : ["wo"],
    "ン" : ["n"]
}

katacrit = {
    "ガ" : ["ga"],
    "ギ" : ["gi"],
    "グ" : ["gu"],
    "ゲ" : ["ge"],
    "ゴ" : ["go"],
    "ザ" : ["za"],
    "ジ" : ["ji"],
    "ズ" : ["zu"],
    "ゼ" : ["ze"],
    "ゾ" : ["zo"],
    "ダ" : ["da"],
    "ヂ" : ["ji"],
    "ヅ" : ["zu"],
    "デ" : ["de"],
    "ド" : ["do"],
    "バ" : ["ba"],
    "ビ" : ["bi"],
    "ブ" : ["bu"],
    "ベ" : ["be"],
    "ボ" : ["bo"],
    "パ" : ["pa"],
    "ピ" : ["pi"],
    "プ" : ["pu"],
    "ペ" : ["pe"],
    "ポ" : ["po"]
}

selected = []
chosenkana = {}
chosenkana2 = {}
score = 0
gotted = ""
lastscore = "Score: 0/0"
finishcheck = 0
nextkey = ""
nextval = [""]
mark = ""

def start(ops):
    global selected, totalscore, chosenkana, chosenkana2
    selected = []
    sel = ops.curselection()
    for i in sel:
        j = ops.get(i)
        if j in selected:
            pass
        else:
            selected.append(j)
    if "Hiragana" in selected:
        if "Monographs" in selected:
            chosenkana = {**chosenkana,**hiragana}
            chosenkana2 = {**chosenkana2,**hiragana}
        if "Diacritics" in selected:
            chosenkana = {**chosenkana,**hiracrit}
            chosenkana2 = {**chosenkana2,**hiracrit}
    if "Katakana" in selected:
        if "Monographs" in selected:
            chosenkana = {**chosenkana,**katakana}
            chosenkana2 = {**chosenkana2,**katakana}
        if "Diacritics" in selected:
            chosenkana = {**chosenkana,**katacrit}
            chosenkana2 = {**chosenkana2,**katacrit}
    if "Hiragana" in selected or "Katakana" in selected:
        if "Monographs" in selected or "Diacritics" in selected:
            for i in root.winfo_children():
                i.place_forget()
            totalscore = len(chosenkana)
            rungame()
    else:
        pass

def getinput(evt):
    global gotted, score, mark
    gotted = userinput.get()
    if gotted == "":
        pass
    else:
        userinput.delete(0,END)
        if gotted in chosenkana2[lastkey]:
            score += 1
            print("Correct")
            mark = "✓"
        else:
            print("Incorrect, correct answer was",str(chosenkana2[lastkey]).strip("[]"))
            mark = "✗"
        get_choice()

def get_choice():
    global lastkey, score, totalscore, lastscore, finishcheck, lastkana, nextkey, nextval, mark, markdisp
    lastkey = random.choice(list(chosenkana.keys()))
    savekey = chosenkana[lastkey]
    chosenkana.pop(lastkey)
    kana.config(text=lastkey)
    lastkanatext = str(nextkey)+" | "+str(nextval[0])
    nextkey = lastkey
    nextval = savekey
    lastkana.config(text=lastkanatext)
    markdisp.config(text=mark)
    if list(chosenkana.keys()) == []:
        finishcheck += 1
        if finishcheck > 1:
            print("You finished! Score:",score,"/",totalscore)
            lastscore = "Score: "+str(score)+"/"+str(totalscore)
            for i in root.winfo_children():
                    i.place_forget()
            mainmenu()
        else:
            chosenkana[lastkey] = savekey
            
root = Tk()
root.geometry("480x480")
root.title("ekKana")
root.config(bg="#edc6d1")
root.resizable(0,0)
root.iconbitmap(r"ekkana.ico")

def mainmenu():
    global ekkana, options, gobutton, lastscore, score, finishcheck
    ekkana = Label(root,text="ekKana",font=("Consolas",56),bg="#edc6d1")
    ekkana.place(relx=0.5,rely=0.2,anchor="center")

    options = Listbox(root,bd=3,selectmode="multiple",height=4,width=12,font=("Consolas",16),selectbackground="#edc6d1")
    options.insert(END,"Monographs")
    options.insert(END,"Diacritics")
    options.insert(END,"Hiragana")
    options.insert(END,"Katakana")
    options.place(relx=0.5,rely=0.5,anchor="center")

    gobutton = Button(root,command=lambda:start(options),text="行こう！",font=("Consolas",16))
    gobutton.place(relx=0.5,rely=0.725,anchor="center")

    prevscore = Label(root,text=lastscore,font=("Consolas",16))
    prevscore.place(relx=0.5,rely=0.875,anchor="center")
    
    score = 0
    finishcheck = 0

def rungame():
    global userinput, kana, lastkana, markdisp
    userinput = Entry(root)
    userinput.config(font=("Consolas",24))
    userinput.focus_set()
    userinput.place(relx=0.5,rely=0.9,anchor="center")
    userinput.bind("<Return>", getinput)

    kana = Label(root, text="", font=("Consolas",86), bd=2, relief="flat")
    kana.place(relx=0.5, rely=0.5, anchor="center")

    lastkana = Label(root,text="|",font=("Consolas",36),bg="#edc6d1")
    lastkana.place(relx=0.5,rely=0.1,anchor="center")

    markdisp = Label(root,text="", font=("Consolas",36),bg="#edc6d1")
    markdisp.place(relx=0.5,rely=0.25,anchor="center")

    get_choice()

mainmenu()

root.mainloop()
