* ekKana requires Python 3.5 at least (I know for a fact it works with 3.6+, but 3.5 is untested)
* Alex Cairns and [Luke Williams](https://gitlab.com/lgwilliams) helped with this project
* exe version does not have window icon because I couldn't get PyInstaller to work
* Make your ekkana.ico is in your root directory